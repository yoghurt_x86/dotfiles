### NOTES:

* Enable multilib! https://wiki.archlinux.org/title/Official_repositories#multilib

* Hibernation can be configured following: https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Hibernation preferably using mkrlconf http://www.rodsbooks.com/refind/linux.html#testing


#### Thinkpad:

* The user should be part of video group for light to be able to adjust backlight
* run `systemctl mask systemd-rfkill.service` and `systemctl mask systemd-rfkill.socket`
* symlinks don't work with logind.conf or sleep.conf so those configs has to be added manual:
	add the following options to /etc/systemd/logind.conf:
```
HandlePowerKey=suspend-then-hibernate
HandleLidSwitch=suspend-then-hibernate
HoldoffTimeoutSec=15s 
```
add the following option to /etc/systemd/sleep.conf
```
HibernateDelaySec=600min
```

#### NAS things:

# NAS is now behind wireguard

You can try and look up ip addres with nmblookup `nmblookup yoghurt-NAS`

to mount NAS use the command: 
```
mkdir -p /media/NAS/photo
chown yoggi /media/NAS
sudo mount -t cifs //yoghurt-NAS/photo /media/NAS/photo -o credentials=/media/NAS/.nasc,vers=3.0
```
where /media/NAS/.nasc has the credentials in the format:
```
username=username
password=password
```
mount.cifs won't be able to resolve the name yoghurt-NAS. fix below, otherwise use static ip.

add 'wins' to /etc/nsswitch.conf: 
```
...
hosts: files wins mymachines myhostname resolve wins [!UNAVAIL=return] dns
...
```
and have winbindd running. 
Remember to configure samba in /etc/samba/smb.conf as mentioned in the wiki:
https://wiki.archlinux.org/index.php/Samba
 
enable/start winbind.service and nmb.service with systemctl
Due to a bug the winbind.service unit has to be updated manually notice that nmbd.service is renamed to nmb.service now!:
https://bugs.launchpad.net/ubuntu/+source/samba/+bug/1789097

`getent -s wins ahosts yoghurt-NAS` to ping yoghurt-NAS netbios name
 
Example for fstab entry:
```
//yoghurt-NAS/photo 	/media/NAS/photo 	cifs credentials=/media/NAS/.nasc,vers=3.0,noauto,user,_netdev 0 0 
```

or using nfs:
yoghurt-NAS:/volume1/photo /NAS/photo nfs _netdev,noauto,x-systemd.automount,x-systemd.mount-timeout=10,timeo=14,x-systemd.idle-timeout=1min 0 0 
yoghurt-NAS:/volume1/homes/userrname /NAS/username nfs _netdev,noauto,x-systemd.automount,x-systemd.mount-timeout=10,timeo=14,x-systemd.idle-timeout=1min 0 0 

### Looks

After installing everything you can run:
`lxappearance` and choose theme and icons for gtk
`qt5ct` and choose theme and icons for qt


