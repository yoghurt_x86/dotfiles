#!/bin/bash

percent=$(/bin/battery_percent)
AC=$(</sys/class/power_supply/AC/online)

bat="%{T2}%{T-}"

if [[ ${AC} -eq 1 ]]; then
	echo "%{u$(xgetres color2)}$bat ${percent}"
elif [[ ${percent} -lt 10 ]]; then	
	echo "%{u$(xgetres color1)}$bat ${percent}"
else
	echo "$bat ${percent}"
fi

