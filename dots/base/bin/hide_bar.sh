#!/bin/bash

result=$(xdotool search --onlyvisible --name "Polybar" )

if [[ $result ]]; then
	xdo hide -N "Polybar"	
else 
	xdo show -N "Polybar"
fi

