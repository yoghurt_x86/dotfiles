# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=6000
SAVEHIST=1000
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/yoggi/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#
alias ls='ls --color=auto'

export PATH="$HOME/bin:$HOME/.emacs.d/bin:$PATH"
export EDITOR="vim"

autoload -Uz promptinit
promptinit
prompt off
