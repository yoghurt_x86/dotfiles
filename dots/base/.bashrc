#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

export PATH="$HOME/bin:$HOME/.emacs.d/bin:$PATH"
export EDITOR="vim"
