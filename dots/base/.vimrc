syntax enable     
set nocompatible

colorscheme Tomorrow-Night-Eighties

set mouse=a
set hidden
set tabstop=4 expandtab
set shiftwidth=4
set softtabstop=0 
set breakindent showbreak=.. 

filetype plugin indent on
set path+=**

set number " Show current line number
set cursorline

set wildmenu " autocomplete commands for example :color <Tab> 
set showmatch " Highlights matching braces

set incsearch 
set hlsearch

" Stop highlights when space is pressed:
nnoremap <leader><space> :noh<CR>

" Copy and paste from clipboard
nnoremap <C-p> "+P
vnoremap <C-c> "+y
vnoremap <C-v> "+p

command Ws w !sudo tee % > /dev/null    
 
" Folding
set foldenable
"set foldlevelstart=0
"set foldnestmax=10
"nnoremap <space> zA
"set foldmethod=syntax "indent   " fold based on indent level

map <C-h> :tabp<CR>
map <C-l> :tabn<CR>
map <C-x> :Files<CR>

set wildignore+=*/node_modules/*
set wildignore+=*/elm-stuff/*


