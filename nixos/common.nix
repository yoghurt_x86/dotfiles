{ config, pkgs, ... }:

{
  config = {

    ## Enable steam
    programs.steam.enable = true;
    # Set your time zone.
    time.timeZone = "Europe/Copenhagen";

    ## Tells nix that we are okay with closed software like Dropbox and Nvidia drivers
    nixpkgs.config.allowUnfree = true;

    ## An overlay for Discord
    nixpkgs.overlays = [(self: super: { discord = super.discord.overrideAttrs (_: {
      src = builtins.fetchTarball {
        url =  "https://discord.com/api/download?platform=linux&format=tar.gz";
        sha256 = "1ahj4bhdfd58jcqh54qcgafljqxl1747fqqwxhknqlasa83li75n";
        }; });})];

    # Select internationalisation properties.
     i18n = {
       defaultLocale = "en_US.UTF-8";
     };

     fonts.fonts = with pkgs; [  ubuntu_font_family terminus_font dejavu_fonts siji ];
     console.font = "Ubuntu Mono";


     # Enable docker
    virtualisation.docker.enable = true;

    # Set the default terminal to URXVTD unicode
    services.urxvtd.enable = true;

    # Enable CUPS to print documents.
     services.printing.enable = true;

    # Enable bluetooth
    # hardware.bluetooth.enable = true;
    # services.blueman.enable = true;
    #hardware.bluetooth.config = {
    #  General = {
    #    Enable = "Source, Sink, Media, Socket";
    #  };
    #};

    # Enable scanner
    # hardware.sane.enable = true;
    # hardware.sane.extraBackends = [ pkgs.sane-backends pkgs.sane-airscan ];

    # Enable sound.
     sound.enable = true;
     hardware.pulseaudio = {
        enable = true;
     #   package = pkgs.pulseaudioFull; # Full package to enable bluetooth supportc
     };

    # Enable the X11 windowing system.
    services.xserver.enable = true;
    ## This ekstra setting might not working in gnome - because gnome overrides it
    #services.xserver.extraLayouts.dk-basic = {
    #  description = "Modified Danish keys to work nicer on english keyboard";
    #  languages   = [ "dk" ];
    #  symbolsFile = ./keys/xps15_dk.txt;
    #};
    services.xserver.layout = "dk";

    # Enable i3
    services.xserver.displayManager.lightdm.enable = true;
    services.xserver.displayManager.defaultSession = "none+i3";
    services.xserver.windowManager.i3 = {
      enable = true;
      package = pkgs.i3-gaps;
      extraPackages = with pkgs;
      let
        polybar = pkgs.polybar.override {
          i3Support = true;
        };

      in 
      [
        dmenu 
        i3lock
        i3status
        polybar
        feh
      ];
    };

    # Define a user account. Don't forget to set a password with ‘passwd’.
    # users.users.olav = {
    #   isNormalUser = true;
    #   extraGroups = [ "wheel" "docker" "networkmanager" "scanner" "lp" "plugdev" ]; # Enable ‘sudo’ for the user and include in other groups
    # };

    environment.systemPackages = with pkgs; [
      wget vim curl git firefox htop tmux neovim stow bash zip unzip vlc
        gparted libsodium pciutils
        libreoffice
        ## A tool for substituting environment variables in file or stdin
        envsubst
        ## For screenshots
        escrotum
        # Scala and tools
        sbt scala ammonite metals
        ## gnupg for encryption
        gnupg
        discord
        ## tomb relies on pinentry for password entry
        pinentry-gtk2
        tomb
        jetbrains.idea-community buku nodejs yarn xclip jdk11
        # Elm stuff
        elmPackages.elm-format elmPackages.elm elmPackages.elm-live
        elmPackages.elm-language-server
        # Docker stuff
        docker-compose
        #kubernetes stuff
        minikube kubectl
        # A utility to get a hash from a git repo
        nix-prefetch-git
        # A fuzzy matcher
        fzf 
        #teams #microsoft teams
        # DVD and video stuff
        lxdvdrip handbrake ddrescue mkvtoolnix
        # poppler has nice pdf utilities such as pdfunite
        poppler poppler_utils
        # Make it possible to operate on NTFS partitions and make bootable windows usb drives
        ntfsprogs woeusb
        nodePackages.prettier
        ## AWS CLI's
        awscli aws-sam-cli amazon-ecs-cli
        heroku zathura
        # Digital ocean command line tool
        doctl
        ## Comic book reader for .cbz files and more
        ##yacreader
        ## Images
        imagemagick
        sxiv # An image viewer
        pinta # simple image editor
        ## Other tools
        openssl
        thunderbird
        mongodb-tools
        youtube-dl
        pulsemixer
        # Custom emacs config
        #(import ./emacs.nix { inherit pkgs; })
        # rclone for onedrive syncing
        # rclone
        # watson httpie jq
        # gitAndTools.gh # github cli client
        ## python stuff
        python3
        # python38Packages.pipx
        # python38Packages.pip
        ## R Stuff
        #(rWrapper.override {
        #  packages = with rPackages;
        #  let 
        #  colorout = buildRPackage{
        #    name= "colorout";
        #    src = pkgs.fetchFromGitHub {
        #      owner = "jalvesaq";
        #      repo = "colorout";
        #      rev = "726d6811b20a5422356abca76f2047058ac35044";
        #      sha256 = "0qrf693nlml09wb4i57qsdp16hx1wvwwqciyllxfsmrx4w9z0v85";
        #    };
        #  };
        #  in
        #    [ ggplot2 dplyr reshape2 knitr readr magrittr mongolite RPostgreSQL
        #  purrr lubridate xtable stringr doBy colorout data_table ];
        #})
        ## Enable the flashing of the moonlander keyboard
        #writeTextFile {
        #  name = "enable-moonlander-flash";
        #  text = ''
        #    # STM32 rules for the Moonlander and Planck EZ
        #    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", \
        #        MODE:="0666", \
        #        SYMLINK+="stm32_dfu"
        #    '';
        #    destination = "/lib/udev/rules.d/50-wally.rules"
        #}
        #
        # Added by me:...
        arandr
        networkmanagerapplet
        stalonetray
        xss-lock
        gnome.gnome-screenshot
        gnome.nautilus
        xfce.tumbler
        ffmpegthumbnailer
        gnome.totem 
        samba
        gvfs
        cifs-utils
        pasystray
        libnotify
        dunst
        xdo
        xdotool
    ];


    # set default shell to be zsh
    users.defaultUserShell = pkgs.bash;
    #programs.zsh.enable = true ;

    ## Enable gnupg agent
    programs.gnupg.agent.enable = true;


    # Setup oh-my-zsh for zsh
    #programs.zsh.ohMyZsh = {
    #  enable=true;
    #  cacheDir="$HOME/.cache/oh-my-zsh";
    #  plugins= [ "git" ];
    #  theme="avit";
    #};

    # Make sure that libsodium is placed as a symlink in the location where
    # the Kalium library in the lendino backend expects it to be

    # Setup a postgres service 
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_12;
      enableTCPIP = false;
      dataDir= "/var/db/postgres";
      initialScript = pkgs.writeText "Initial-Postgres-script" ''
        CREATE USER yoggi CREATEDB SUPERUSER;
        ALTER USER yoggi WITH PASSWORD 'uggabugga';

        CREATE DATABASE locallendinodev ENCODING = 'UTF8' TEMPLATE='template0';
        CREATE DATABASE locallendinotest ENCODING = 'UTF8' TEMPLATE='template0';
        \connect locallendinodev;
        ALTER SCHEMA public OWNER TO yoggi;
        \connect locallendinotest;
        ALTER SCHEMA public OWNER TO yoggi;
      '';
    };

   # Setup a mongo service
   services.mongodb = {
      enable = true;
      dbpath = "/var/db/mongodb";
      enableAuth = true;
      package = pkgs.mongodb-4_2;
      initialRootPassword = "uggabugga";
   };

    # Standard environment variables
    environment.variables = { EDITOR = "nvim"; };

    # Add some neovim config stuff
  };
}

