#!/bin/bash

# make sure we have pulled in and updated any submodules
#git submodule init
#git submodule update

# what directories should be installable by all users including the root user
#base=(
#    bash
#)

# folders that should, or only need to be installed for a local user
#useronly=(
#    git
#)

user=(
	base
	thinkpad-base
)

global=(
	global
	thinkpad-global
)


install-pack() {
	list=$1
	echo "installing ${list}"
	sudo pacman -Syyy
	sudo pacman -S - < ${list}
}

for name in apps/*; do
while true; do
    read -p "Do you wish to install ${name} packages? [Y/n]" yn
    case $yn in
        [Yy]* ) install-pack ${name}; break;;
        [Nn]* ) echo "Skipping install..."; break;;
		"")     install-pack ${name}; break;;
        * ) 	echo "Please answer yes or no.";;
    esac
done
done

install-aur() {
	list=$1
	if ! command -v pikaur; then
		git clone https://aur.archlinux.org/pikaur.git ~/pikaur-build
		cd ~/pikaur-build
		makepkg -fsri
		cd -
		rm -rf ~/pikaur-build
	fi	
	for aur in $(cat ${list}); do
		echo "$aur"
		pikaur -S $aur
	done
}

for name in aur/*; do
while true; do
    read -p "Do you wish to install AUR ${name} packages? [Y/n]" yn
    case $yn in
        [Yy]* ) install-aur ${name}; break;;
        [Nn]* ) echo "Skipping install..."; break;;
		"")     install-aur ${name}; break;;
        * ) 	echo "Please answer yes or no.";;
    esac
done
done


stowit() {
    usr=$1
    app=$2
    # -v verbose
    # -R recursive
    # -t target
    stow -v -R -t ${usr} ${app}
}


stowglo() {
    usr=$1
    app=$2
    sudo stow -v -R -t ${usr} ${app}
}

echo ""
echo "Stowing home configs for user: ${whoami}"
cd dots

# install apps available to local users and root
for app in ${user[@]}; do
while true; do
    echo ${app}
    read -p "Do you wish to stow ${app} to ${HOME}? [Y/n]" yn
    case $yn in
        [Yy]* ) stowit "${HOME}" ${app}; break;;
        [Nn]* ) echo "Skipping install..."; break;;
		"")     stowit "${HOME}" ${app}; break;;
        * ) 	echo "Please answer yes or no.";;
    esac
done
done

cd ..
cd dots

echo ""
echo "Stowing global configs"
# install apps available to local users and root
for app in ${global[@]}; do
while true; do
    read -p "Do you wish to stow ${app} at / ? [Y/n]" yn
    case $yn in
        [Yy]* ) stowglo "/" ${app}; break;;
        [Nn]* ) echo "Skipping install..."; break;;
		"")     stowglo "/" ${app}; break;;
        * ) 	echo "Please answer yes or no.";;
    esac
done
done

cd ..

service() {
	services=$1
	#while read s; do
		#sudo systemctl enable $s
	#done < ${services}
	for service in $(cat ${services}); do
		echo "${service}"
		sudo systemctl enable ${service}
	done
}

echo ""
echo "Enabling services!"
for name in services/*; do
while true; do
    read -p "Do you wish to enable ${name} services? [Y/n]" yn
    case $yn in
        [Yy]* ) service ${name}; break;;
        [Nn]* ) echo "Skipping services..."; break;;
		"")     service ${name}; break;;
        * ) 	echo "Please answer yes or no.";;
    esac
done
done


echo ""
echo "##### ALL DONE"

